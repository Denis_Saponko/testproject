import { Component } from '@angular/core';
import { Banner } from './banner';
import { BannerService } from './banner.service';
import { filter } from 'minimatch';
import { style } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appTitle: string = 'Banner generator v1.0.2';

  banners: Banner[] = [];
  newBanner: Banner;
  currentBlockId = null;
  isFullBannerList = false;
  wBanner: string = '300px';
  wHeight: string = '150px';
  closeMenu = false;
  currentBlockColor: string ='#eaeaea';
  currentBlockWidth: string;
  currentBlockHeight: string;
  isFullList :boolean = false;


  getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }


  CreateBanner() {
    if(this.banners.length >= 9) {
      this.isFullList = true;
    }
    this.newBanner = {
      id: this.banners.length + 1,
      width: this.wBanner,
      height: this.wHeight,
      bColor: 'yellow',
      isShoosed: false
    }
    this.banners.push(this.newBanner);
  }

  chooseBanner(id: number) {
    this.currentBlockId = id;
    let el = document.getElementById(id.toString());
    let dragAndDropEl = el.getElementsByTagName('div')[0].style.transform;
    let banner = this.banners.find(item => item.id == id);
    this.currentBlockColor = banner.bColor;
    this.currentBlockWidth = banner.width;
    this.currentBlockHeight = banner.height;
    this.setCurrentColor();
    this.setWidth(id);
    this.setHeight(id);
    this.closeMenu = true;
  }

  DeleteBanner(id: number) {
    let banner = this.banners.find(item => item.id == id);
    var index = this.banners.indexOf(banner);
    if (index > -1) {
      this.banners.splice(index, 1);
      this.currentBlockId = null;
      this.closeMenu = false;
      this.isFullList = false;
    }
  }

  ChangeColor(id: number) {
    let banner = this.banners.find(item => item.id == id);
    banner.bColor = this.getRandomColor();
    this.currentBlockColor = banner.bColor;
    this.setCurrentColor();
  }
  setCurrentColor() {
    const styles = {
      'background-color': this.currentBlockColor
    }
    return styles;
  }

setWidth(id) {
  let banner = this.banners.find(item => item.id == id);
  return banner.width;
}
reWidth(id,event) {

  if(event.key == 'Enter') {
    let banner = this.banners.find(item => item.id == id);
    banner.width = this.currentBlockWidth+'px';
    return banner.width;
  } 
  return null;
}


setHeight(id) {
  let banner = this.banners.find(item => item.id == id);
  return banner.height;
}
reHeight(id,event) {

  if(event.key == 'Enter') {
    let banner = this.banners.find(item => item.id == id);
    banner.height = this.currentBlockHeight+'px';
    return banner.height;
  } 
  return null;
}

}



