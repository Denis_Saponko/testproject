import { Component, OnInit, Input } from '@angular/core';
import { Banner } from '../banner';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  constructor() { }
  
  @Input() banner;
  
  ngOnInit() {
  }

}
